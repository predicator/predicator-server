var http = require('http'),
    router = require('./router'),
    config = require('./config');

var server = http.createServer(router.createHandler());

server.listen(config.port);