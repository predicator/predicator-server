var config = require('../config'),
    gfc = require('gfc-node')();

function create(gfcToken, recordData, callback){
    gfc.records.create(
        gfcToken,
        recordData,
        function(error, document){
            if(error){
                return callback(error);
            }
            document.data._id = document.id;
            callback(null, document);
        }
    );
}

function update(gfcToken, documentId, recordData, callback){
    gfc.records.update(
        gfcToken,
        documentId,
        recordData,
        function(error, document){
            if(error){
                return callback(error);
            }
            document.data._id = document.id;
            callback(null, document);
        }
    );
}
    
function get(gfcToken, documentId, callback){
    gfc.records.get(
        gfcToken,
        documentId,
        function(error, document){
            if(error){
                return callback(error);
            }
            document.data._id = document.id;
            callback(null, document);
        }
    );
}

function getAll(gfcToken, callback){
    gfc.records.getAll(
        gfcToken,
        {
            skip: 0,
            limit: 5,
            query: {
                where: {
                    type: 'document'
                }
            }
        },
        function(error, documents){
            if(error){
                return callback(error);
            }

            var records = documents.reverse();

            callback(null, records.map(function(document){
                document.data._id = document.id;
                return document.data;
            }));
        }
    );
}

module.exports = {
    get: get,
    getAll: getAll,
    create: create,
    update: update
};