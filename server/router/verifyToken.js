module.exports = function(routeHandler){
    return routeHandler;
};

return;

var services = require('../services');

function verifyToken(routeHandler){
    return function(request, response){
    
        function unauthorised(response){
            response.writeHead(401);
            response.end('Unauthorised');
        }

        if(!request.headers || !request.headers.authorization){

            unauthorised(response);

            return;
        }

        services.authentication.validateToken(
            request.headers.authorization.split(' ').pop(),
            function(error, account){
                if(error || !account){

                    unauthorised(response);

                    return;
                }

                request.account = account;

                routeHandler(request, response);
            }
        );
    };
}


module.exports = verifyToken;
