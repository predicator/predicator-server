var requestData = require('request-data'),
    documentService = require('../services').document;

function update(request, response, tokens, data){
    var record = {};
    record.data = data.data;
    record.schemaId = data.schemaId;
    
    documentService.update(request.headers.authorization, tokens.documentId, record, function(error, document){
        console.log(error);
        if(error){
            response.writeHead(500);
            response.end(JSON.stringify(error.message));
            return;
        }

        response.end(JSON.stringify(document));
    });
}

function create(request, response, tokens, data){
    documentService.create(request.headers.authorization, data, function(error, document){

        if(error){
            response.writeHead(500);
            response.end(JSON.stringify(error.message));
            return;
        }

        response.end(JSON.stringify(document));
    });
}

function getAll(request, response){
    documentService.getAll(request.headers.authorization, function(error, documents){

        if(error){
            response.writeHead(500);
            response.end(JSON.stringify(error.message));
            return;
        }

        response.end(JSON.stringify(documents));
    });
}

function get(request, response, tokens){
    documentService.get(request.headers.authorization,tokens.documentId, function(error, document){

        if(error){
            response.writeHead(404);
            response.end(JSON.stringify(error.message));
            return;
        }

        response.end(JSON.stringify(document));
    });
}

module.exports = {
    '/documents': {
        POST: requestData(create),
        GET: getAll
    },
    '/documents/`documentId`': {
        GET: get,
        PUT: requestData(update)
    }
};