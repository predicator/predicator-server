var documentPersistence = require('../../persistence/document');

function get(gfcToken, documentId, callback){
    if(documentId === ''){
        return callback(new Error('Not found'));
    }

    documentPersistence.get(gfcToken, documentId, callback);
}

function getAll(gfcToken, callback){
    documentPersistence.getAll(gfcToken, callback);
}

function create(gfcToken, data, callback){
    documentPersistence.create(gfcToken, data, callback);
}

function update(gfcToken, documentId, data, callback){
    if(documentId === ''){
        return callback(new Error('Not found'));
    }

    documentPersistence.update(gfcToken, documentId, data, callback);
}

module.exports = {
    get: get,
    getAll: getAll,
    create: create,
    update: update
};