# Predicator Server

Basic server app using GFC to accompany the Predicator Client app

## Goals

The goals are to demonstrate an app which uses GFC in the back-end. 

## Requirements

The app server will 

* Authenticate app with gfc
* Authenticate app-user for login to app which uses gfc
* Filter returns from gfc to respond to requests from the app-client